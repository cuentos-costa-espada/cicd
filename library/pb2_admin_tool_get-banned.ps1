﻿param 
( 
    [string]$iniPath=""
)

# Variables generales
$ips = [ordered]@{}
$players = [ordered]@{}
$cdkeys = [ordered]@{}
$sectionReding = ""

# Revisamos estado actual de baneados
foreach($line in Get-Content $iniPath) {

    if($line -eq "[Banned Ips]" -or $line -eq "[Banned Players]" -or $line -eq "[Banned CD Keys]") { $sectionReding = "$line" }
    elseif($line -eq "" -or $line.StartsWith("[") -and $sectionReding -ne "" -and $line -ne "[Banned Ips]" -and $line -ne "[Banned Players]" -and $line -ne "[Banned CD Keys]") { break }
    elseif($sectionReding -eq "[Banned Ips]") {

        $key = $line.Split("=")[0]
        $ip = $line.Split("=")[1]
        if($ip -ne "") { $ips.Add($key, $ip) > $null }
    }
    elseif($sectionReding -eq "[Banned Players]") {

        $key = $line.Split("=")[0]
        $player = $line.Split("=")[1]
        if($player -ne "") { $players.Add($key, $player) > $null }
    }
    elseif($sectionReding -eq "[Banned CD Keys]") {

        $key = $line.Split("=")[0]
        $cdkey = $line.Split("=")[1]
        if($cdkey -ne "") { $cdkeys.Add($key, $cdkey) > $null }
    }
}

# Imprimimos el estado de los baneados
Write-Output ""
Write-Output "+---------------------+"
Write-Output "| LISTADO DE BANEADOS |"
Write-Output "+---------------------+"
Write-Output ""
$count = $players.Count
Write-Output "Cuentas baneadas ($count):"
if($count -gt 0) {
    $players.keys | ForEach-Object{
        $value = $players[$_]
        Write-Output "* $value"
    }
} else {
    Write-Output "* N/A"
}
Write-Output ""
$count = $cdkeys.Count
Write-Output "CDKEYs baneadas ($count):"
if($count -gt 0) {
    $cdkeys.keys | ForEach-Object{
        $value = $cdkeys[$_]
        Write-Output "* $value"
    }
} else {
    Write-Output "* N/A"
}
Write-Output ""
$count = $ips.Count
Write-Output "IPs baneadas ($count):"
if($count -gt 0) {
    $ips.keys | ForEach-Object{
        $value = $ips[$_]
        Write-Output "* $value"
    }
} else {
    Write-Output "* N/A"
}

Write-Output ""