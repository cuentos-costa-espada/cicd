﻿param 
( 
    [string]$BanCuenta="",
    [string]$BanCDKEY="",
    [string]$BanIP="",
    [string]$iniPath="",
    [string]$iniTmpPath=""
)

# Variables generales
$ips = [ordered]@{}
$players = [ordered]@{}
$cdkeys = [ordered]@{}
$needPlayerEdit = "false"
$needCDKEYEdit = "false"
$needIPEdit = "false"
$sectionReding = ""
$firsLine = 0
$lastLine = 0
$index = 1

# Revisamos estado actual de baneados
foreach($line in Get-Content $iniPath) {

    if($line -eq "[Banned Ips]" -or $line -eq "[Banned Players]" -or $line -eq "[Banned CD Keys]") {

        $sectionReding = "$line"
        if($firsLine -eq 0) { $firsLine = $index } 
    }
    elseif($line -eq "" -or $line.StartsWith("[") -and $sectionReding -ne "" -and $line -ne "[Banned Ips]" -and $line -ne "[Banned Players]" -and $line -ne "[Banned CD Keys]") { break }
    elseif($sectionReding -eq "[Banned Ips]") {

        $key = $line.Split("=")[0]
        $ip = $line.Split("=")[1]
        if($ip -ne "") { $ips.Add($key, $ip) > $null }
    }
    elseif($sectionReding -eq "[Banned Players]") {

        $key = $line.Split("=")[0]
        $player = $line.Split("=")[1]
        if($player -ne "") { $players.Add($key, $player) > $null }
    }
    elseif($sectionReding -eq "[Banned CD Keys]") {

        $key = $line.Split("=")[0]
        $cdkey = $line.Split("=")[1]
        if($cdkey -ne "") { $cdkeys.Add($key, $cdkey) > $null }
    }

    $lastLine = $index
    $index++
}

# Revisar si es necesario modificar el .ini
if($BanCuenta -ne "" -and $BanCuenta -notin $players.Values) { $needPlayerEdit = "true" }
if($BanCDKEY  -ne "" -and $BanCDKEY  -notin $cdkeys.Values)  { $needCDKEYEdit  = "true" }
if($BanIP     -ne "" -and $BanIP     -notin $ips.Values)     { $needIPEdit     = "true" }

if($needPlayerEdit -eq "false" -and $needCDKEYEdit -eq "false" -and $needIPEdit -eq "false") { exit 0 }

# Confeccionar nuevo bloque de baneados
$newBlock = "[Banned Players]"
$index = 0
$players.keys | ForEach-Object{
    $key = $_
    $value = $players[$_]
    $newBlock += "`r`n$key=$value"
    $index++
}
if($needPlayerEdit -eq "true") {
    $newBlock += "`r`n$index=$BanCuenta"
    $index++
    $newBlock += "`r`n$index="

} else {
    $newBlock += "`r`n$index="
}

$newBlock += "`r`n[Banned CD Keys]"
$index = 0
$cdkeys.keys | ForEach-Object{
    $key = $_
    $value = $cdkeys[$_]
    $newBlock += "`r`n$key=$value"
    $index++
}
if($needCDKEYEdit -eq "true") {
    $newBlock += "`r`n$index=$BanCDKEY"
    $index++
    $newBlock += "`r`n$index="

} else {
    $newBlock += "`r`n$index="
}

$newBlock += "`r`n[Banned Ips]"
$index = 0
$ips.keys | ForEach-Object{
    $key = $_
    $value = $ips[$_]
    $newBlock += "`r`n$key=$value"
    $index++
}
if($needIPEdit -eq "true") {
    $newBlock += "`r`n$index=$BanIP"
    $index++
    $newBlock += "`r`n$index="

} else {
    $newBlock += "`r`n$index="
}

$newBlock += "`r`n"

# Escribir en el fichero .ini
Get-content $iniPath | Where-Object readcount -notin ($firsLine..$lastLine) | set-content $iniTmpPath
$newFile = Get-content $iniTmpPath
$newFile[$firsLine-1] = $newBlock + $newFile[$firsLine-1]
$newFile | Set-Content $iniPath
Remove-Item -Path $iniTmpPath -Force
