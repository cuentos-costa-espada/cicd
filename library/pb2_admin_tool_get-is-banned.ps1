﻿param 
( 
    [string]$BanCuenta="",
    [string]$BanCDKEY="",
    [string]$BanIP="",
    [string]$iniPath=""
)

# Variables generales
$ips = [ordered]@{}
$players = [ordered]@{}
$cdkeys = [ordered]@{}
$foundPlayer = "false"
$foundCDKEY = "false"
$foundIP = "false"
$sectionReding = ""

# Revisamos estado actual de baneados
foreach($line in Get-Content $iniPath) {

    if($line -eq "[Banned Ips]" -or $line -eq "[Banned Players]" -or $line -eq "[Banned CD Keys]") { $sectionReding = "$line" }
    elseif($line -eq "" -or $line.StartsWith("[") -and $sectionReding -ne "" -and $line -ne "[Banned Ips]" -and $line -ne "[Banned Players]" -and $line -ne "[Banned CD Keys]") { break }
    elseif($sectionReding -eq "[Banned Ips]") {

        $key = $line.Split("=")[0]
        $ip = $line.Split("=")[1]
        if($ip -ne "") { $ips.Add($key, $ip) > $null }
    }
    elseif($sectionReding -eq "[Banned Players]") {

        $key = $line.Split("=")[0]
        $player = $line.Split("=")[1]
        if($player -ne "") { $players.Add($key, $player) > $null }
    }
    elseif($sectionReding -eq "[Banned CD Keys]") {

        $key = $line.Split("=")[0]
        $cdkey = $line.Split("=")[1]
        if($cdkey -ne "") { $cdkeys.Add($key, $cdkey) > $null }
    }
}

# Revisar si es necesario modificar el .ini
if($BanCuenta -ne "" -and $BanCuenta -in $players.Values) { $foundPlayer = "true" }
if($BanCDKEY  -ne "" -and $BanCDKEY  -in $cdkeys.Values)  { $foundCDKEY  = "true" }
if($BanIP     -ne "" -and $BanIP     -in $ips.Values)     { $foundIP     = "true" }

if($foundPlayer -eq "false" -and $foundCDKEY -eq "false" -and $foundIP -eq "false") { Write-Output "false" }
else { Write-Output "true" }

exit 0