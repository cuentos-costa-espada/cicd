﻿param 
( 
    [string]$BanCuenta="",
    [string]$BanCDKEY="",
    [string]$BanIP="",
    [string]$iniPath="",
    [string]$iniTmpPath=""
)

# Variables generales
$ips = [ordered]@{}
$players = [ordered]@{}
$cdkeys = [ordered]@{}
$needPlayerEdit = "false"
$needCDKEYEdit = "false"
$needIPEdit = "false"
$sectionReding = ""
$firsLine = 0
$lastLine = 0
$index = 1

# Revisamos estado actual de baneados
foreach($line in Get-Content $iniPath) {

    if($line -eq "[Banned Ips]" -or $line -eq "[Banned Players]" -or $line -eq "[Banned CD Keys]") {

        $sectionReding = "$line"
        if($firsLine -eq 0) { $firsLine = $index } 
    }
    elseif($line -eq "" -or $line.StartsWith("[") -and $sectionReding -ne "" -and $line -ne "[Banned Ips]" -and $line -ne "[Banned Players]" -and $line -ne "[Banned CD Keys]") { break }
    elseif($sectionReding -eq "[Banned Ips]") {

        $key = $line.Split("=")[0]
        $ip = $line.Split("=")[1]
        if($ip -ne "") { $ips.Add($key, $ip) > $null }
    }
    elseif($sectionReding -eq "[Banned Players]") {

        $key = $line.Split("=")[0]
        $player = $line.Split("=")[1]
        if($player -ne "") { $players.Add($key, $player) > $null }
    }
    elseif($sectionReding -eq "[Banned CD Keys]") {

        $key = $line.Split("=")[0]
        $cdkey = $line.Split("=")[1]
        if($cdkey -ne "") { $cdkeys.Add($key, $cdkey) > $null }
    }

    $lastLine = $index
    $index++
}

# Revisar si es necesario modificar el .ini
if($BanCuenta -ne "" -and $BanCuenta -in $players.Values) { $needPlayerEdit = "true" }
if($BanCDKEY  -ne "" -and $BanCDKEY  -in $cdkeys.Values)  { $needCDKEYEdit  = "true" }
if($BanIP     -ne "" -and $BanIP     -in $ips.Values)     { $needIPEdit     = "true" }

if($needPlayerEdit -eq "false" -and $needCDKEYEdit -eq "false" -and $needIPEdit -eq "false") { exit 0 }

# Confeccionar nuevo bloque de baneados
$newBlock = "[Banned Players]"
$index = 0
$players.keys | ForEach-Object{
    $value = $players[$_]
    if($needPlayerEdit -eq "true" -and $value -eq $BanCuenta) {
        # Nothing...
    } else {
        $newBlock += "`r`n$index=$value"
        $index++
    }
}
$newBlock += "`r`n$index=`r`n"

$newBlock += "[Banned CD Keys]"
$index = 0
$cdkeys.keys | ForEach-Object{
    $value = $cdkeys[$_]
    if($needCDKEYEdit -eq "true" -and $value -eq $BanCDKEY) {
        # Nothing...
    } else {
        $newBlock += "`r`n$index=$value"
        $index++
    }
}
$newBlock += "`r`n$index=`r`n"

$newBlock += "[Banned Ips]"
$index = 0
$ips.keys | ForEach-Object{
    $value = $ips[$_]
    if($needIPEdit -eq "true" -and $value -eq $BanIP) {
        # Nothing...
    } else {
        $newBlock += "`r`n$index=$value"
        $index++
    }
}
$newBlock += "`r`n$index=`r`n"

# Escribir en el fichero .ini
Get-content $iniPath | Where-Object readcount -notin ($firsLine..$lastLine) | set-content $iniTmpPath
$newFile = Get-content $iniTmpPath
$newFile[$firsLine-1] = $newBlock + $newFile[$firsLine-1]
$newFile | Set-Content $iniPath
Remove-Item -Path $iniTmpPath -Force
